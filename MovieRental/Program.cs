﻿using System;
using System.Collections.Generic;

namespace MovieRentalKata
{
    enum PriceCode
    {
        REGULAR,
        NEW_RELEASE,
        CHILDRENS
    }

    class Movie
    {
        private String title;
        private PriceCode priceCode;

        public Movie(String title, PriceCode priceCode)
        {
            this.title = title;
            this.priceCode = priceCode;
        }

        public PriceCode PriceCode
        {
            get => priceCode;
            set => priceCode = value;
        }

        public String Title
        {
            get => title;
        }
    }

    class Rental
    {
        private Movie movie;
        private int daysRented;

        public Rental(Movie movie, int daysRented)
        {
            this.movie = movie;
            this.daysRented = daysRented;
        }

        public int DaysRented
        {
            get => daysRented;
        }

        public Movie Movie
        {
            get => movie;
        }
        public double GetCharge()
        {
            double result = 0;
            // Determine amounts for each line
            switch (this.Movie.PriceCode)
            {


                case PriceCode.REGULAR:
                    result += 2;
                    if (this.DaysRented > 2)
                        result += (this.DaysRented - 2) * 1.5;
                    break;

                case PriceCode.NEW_RELEASE:
                    result += this.DaysRented * 3;
                    break;

                case PriceCode.CHILDRENS:
                    result += 1.5;
                    if (this.DaysRented > 3)
                        result += (this.DaysRented - 3) * 1.5;
                    break;
            }
            return result;
        }
    }

    class Customer
    {
        private String name;
        private List<Rental> rentals = new List<Rental>();

        public Customer(String name)
        {
            this.name = name;
        }

        public void AddRental(Rental rental)
        {
            rentals.Add(rental);
        }

        public String Name
        {
            get => name;
        }

        public String Statement()
        {
            String result = "Rental Record for " + Name + "\n";

            foreach (var rental in rentals)
            {
                // Show figures for this rental
                result += "\t" + rental.Movie.Title + "\t" + rental.GetCharge().ToString() + "\n";
            }

            // Add footer lines
            result += "Amount owed is " + GetTotalCharge().ToString() + "\n";
            result += "You earned " + GetTotalFrequentRenterPoints().ToString() + " frequent renter points";

            return result;
        }

        public String HTMLStatement()
        {
            String result = "<h1>Rental Record for <em>" + Name + "</em></h1>\n";
            result += "<0l>\n";

            foreach (var rental in rentals)
            {
                // Show figures for this rental
                result += "<li><em>" + rental.Movie.Title + "</em>:" + rental.GetCharge().ToString() + "</li>\n";
            }
            result += "</ol>\n";
            // Add footer lines
            result += "<p>";
            result += "Amount owed is <em>" + GetTotalCharge().ToString() + "</em>\n";
            result += "</p>";

            result += "<p>";
            result += "You earned <em>" + GetTotalFrequentRenterPoints().ToString() + "</em> frequent renter points";
            result += "</p>";
            return result;
        }

        public double GetTotalCharge()
        {
            double totalAmount = 0;

            foreach (var rental in rentals)
            {
                totalAmount += rental.GetCharge();
            }

            return totalAmount;

        }
        public int GetTotalFrequentRenterPoints()
        {
            int frequentRenterPoints = 0;

            foreach (var rental in rentals)
            {
                frequentRenterPoints += GetFrequentRenterPoints(rental);

            }

            return frequentRenterPoints;

        }

        private static int GetFrequentRenterPoints(Rental rental)
        {
            // Add frequent renter points
            int result = 1;


            // Add bonus for a two day new release rental
            if ((rental.Movie.PriceCode == PriceCode.NEW_RELEASE) && rental.DaysRented > 1)
                result += 1;
            return result;
        }

        class MainClass
        {
            public static void Main(string[] args)
            {
                var movie01 = new Movie("Captain Marvel", PriceCode.NEW_RELEASE);
                var movie02 = new Movie("Aladdin", PriceCode.CHILDRENS);
                var movie03 = new Movie("Back To The Future", PriceCode.REGULAR);

                var customer = new Customer("Roger Ebert");

                var rental01 = new Rental(movie01, 2);
                var rental02 = new Rental(movie02, 3);
                var rental03 = new Rental(movie03, 2);

                customer.AddRental(rental01);
                customer.AddRental(rental02);
                customer.AddRental(rental03);

                Console.WriteLine(customer.HTMLStatement());

            }
        }

    }
}

